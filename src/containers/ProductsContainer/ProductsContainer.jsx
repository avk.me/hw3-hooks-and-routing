import React from "react";
import { useState, useEffect } from "react";
import { ProductsContext } from "../../components/ProductsContext/ProductsContext";

function ProductsContainer(props) {
  const [products, setProducts] = useState([]);

  const [productsToCartId, setProductsToCart] = useState(
    localStorage.getItem("cart")?.split(",") || []
  );

  const [favoriteProductsId, setFavoriteProducts] = useState(
    localStorage.getItem("favorites")?.split(",") || []
  );

  const [cardToCartId, setCardToCart] = useState("");

  const { children } = props;

  useEffect(() => {
    fetch("./products.json")
      .then((response) => response.json())
      .then((products) => {
        setProducts(products);
      })
      .catch(() => {
        console.log("There are some problems with data downloading...");
      });
  }, []);

  useEffect(() => {
    localStorage.removeItem("cart");

    if (productsToCartId.length > 0) {
      localStorage.setItem("cart", productsToCartId);
    }
  }, [productsToCartId]);

  useEffect(() => {
    localStorage.removeItem("favorites");

    if (favoriteProductsId.length > 0) {
      localStorage.setItem("favorites", favoriteProductsId);
    }
  }, [favoriteProductsId]);

  const addToCart = () => {
    if (!productsToCartId.includes(cardToCartId)) {
      setProductsToCart([...productsToCartId, cardToCartId]);
    }

    closeModal();
  };

  const removeFromCart = (id) => {
    const newProductsToCart = productsToCartId.filter((productId) => {
      return productId !== id;
    });

    setProductsToCart(newProductsToCart);
  };

  const addToFavorites = (id) => {
    setFavoriteProducts([...favoriteProductsId, id]);
  };

  const removeFromFavorites = (id) => {
    const newFavorites = favoriteProductsId.filter((productId) => {
      return productId !== id;
    });

    setFavoriteProducts(newFavorites);
  };

  const openModal = (id) => {
    setCardToCart(id);
  };

  const closeModal = () => {
    setCardToCart("");
  };

  return (
    <ProductsContext.Provider
      value={{
        productList: products,
        favoriteProductsId: favoriteProductsId,
        productsToCartId: productsToCartId,
        cardToCartId: cardToCartId,
        openModal: openModal,
        closeModal: closeModal,
        addToFavorites: addToFavorites,
        removeFromFavorites: removeFromFavorites,
        addToCart: addToCart,
        removeFromCart: removeFromCart,
      }}
    >
      {children}
    </ProductsContext.Provider>
  );
}

export default ProductsContainer;
