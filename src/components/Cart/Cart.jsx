import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import ProductCard from "../ProductCard/ProductCard";
import { ProductsContext, productsToShow } from "../ProductsContext/ProductsContext";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";

function Cart() {
  const {
    productList,
    productsToCartId,
    favoriteProductsId,
    cardToCartId,
    openModal,
    closeModal,
    addToFavorites,
    removeFromFavorites,
    removeFromCart,
  } = useContext(ProductsContext);

  const [productsToCart, setProductsToCart] = useState([]);

  useEffect(() => {
      setProductsToCart(productsToShow(productList, productsToCartId));
  }, [productList, productsToCartId]);

  const removeProductFromCart = (e) => {
    removeFromCart(e.target.dataset.id);
    closeModal();
  };

  return (
    <>
      <Link to={"/"}>
        <Button text="HOME" color="grey"></Button>
      </Link>
      <Link to={"/favorites"}>
        <Button text="FAVORIRES" color="gold"></Button>
      </Link>
      <Link to={"/cart"}>
        <Button text="CART" color="green"></Button>
      </Link>

      <div style={{ minWidth: "100%", textAlign: "center" }}>
        {!productsToCart.length && (
          <div style={{ fontSize: "30px" }}>
            Currently, there are no products in cart
          </div>
        )}

        {productsToCart.map((product) => {
          return (
            <>
              <ProductCard
                key={product.id}
                id={product.id}
                image={product.favorites}
                name={product.name}
                color={product.color}
                code={product.code}
                price={product.price}
                openModal={openModal}
                addToFavorites={addToFavorites}
                removeFromFavorites={removeFromFavorites}
                isFavorite={
                  favoriteProductsId.includes(product.id) ? true : false
                }
                btnColor={"red"}
                btnText={"Remove from cart"}
              />
            </>
          );
        })}
      </div>

      {cardToCartId !== "" && (
        <>
          <div onClick={closeModal} className="blackback"></div>
          <Modal
            header="Do you want to delete this?"
            text="When you click 'Ok' this car will be deleted from your cart"
            color="red"
            closeModal={closeModal}
            actions={
              <div className="modal-btns">
                <Button
                  text="Ok"
                  func={removeProductFromCart}
                  color="black"
                  data-id={cardToCartId}
                ></Button>
                <Button text="Cancel" func={closeModal} color="grey"></Button>
              </div>
            }
          />
        </>
      )}
    </>
  );
}

export default Cart;
