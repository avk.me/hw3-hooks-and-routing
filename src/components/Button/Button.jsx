import React, { useContext } from 'react';
import PropTypes from "prop-types";
import "./Button.scss";
import { ProductsContext } from '../ProductsContext/ProductsContext';

function Button (props) {
    const { cardToCartId } = useContext(ProductsContext)
    const { color, text, func } = props;
    
    return (
        <a data-id={cardToCartId} className="button" onClick={func} style={{ backgroundColor: color }}>{text}</a>
    )
    
}

Button.propTypes = {
    func: PropTypes.func,
    text: PropTypes.string.isRequired,
    color: PropTypes.string,
}

Button.defaultProps = {
    color: "red"
}

export default Button;