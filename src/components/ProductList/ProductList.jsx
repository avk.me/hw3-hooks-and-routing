import { useContext } from "react";
import React from "react";
import { Link } from "react-router-dom";
import ProductCard from "../ProductCard/ProductCard";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import { ProductsContext } from "../ProductsContext/ProductsContext";

function ProductList() {
  const {
    productList,
    favoriteProductsId,
    cardToCartId,
    openModal,
    closeModal,
    addToFavorites,
    removeFromFavorites,
    addToCart,
  } = useContext(ProductsContext);

  return (
    <>
      <Link to={"/"}>
        <Button text="HOME" color="grey"></Button>
      </Link>
      <Link to={"/favorites"}>
        <Button text="FAVORIRES" color="gold"></Button>
      </Link>
      <Link to={"/cart"}>
        <Button text="CART" color="green"></Button>
      </Link>

      <div>
        {productList.map((product) => {
          return (
            <ProductCard
              key={product.id}
              id={product.id}
              image={product.img}
              name={product.name}
              color={product.color}
              code={product.code}
              price={product.price}
              openModal={openModal}
              addToFavorites={addToFavorites}
              removeFromFavorites={removeFromFavorites}
              isFavorite={
                favoriteProductsId.includes(product.id) ? true : false
              }
              btnColor={"green"}
              btnText={"Add to cart"}
            />
          );
        })}
      </div>

      {cardToCartId !== "" && (
        <>
          <div onClick={closeModal} className="blackback"></div>
          <Modal
            header="Do you want to add this?"
            text="When you click 'Ok' this car will be added to your cart"
            color="green"
            closeModal={closeModal}
            actions={
              <div className="modal-btns">
                <Button text="Ok" func={addToCart} color="black"></Button>
                <Button text="Cancel" func={closeModal} color="grey"></Button>
              </div>
            }
          />
        </>
      )}
    </>
  );
}

export default ProductList;
