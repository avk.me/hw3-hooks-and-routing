import React from "react";

export const ProductsContext = React.createContext({
  productList: [],
  favoriteProductsId: [],
  productsToCartId: [],
  cardToCartId: null,
  openModal: () => {},
  closeModal: () => {},
  addToFavorites: () => {},
  removeFromFavorites: () => {},
  addToCart: () => {},
  removeFromCart: () => {},
});

export const productsToShow = (productList, productsToShowId) => {
  const products = [];
  productList.forEach((product) => {
    productsToShowId.forEach((productToShowId) => {
      if (product.id === productToShowId) {
        products.push(product);
      }
    });
  });

  return products;
};
