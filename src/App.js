import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.scss";
import ProductList from "./components/ProductList/ProductList.jsx";
import Favorites from "./components/Favorites/Favorites";
import Cart from "./components/Cart/Cart";
import ProductsContainer from "./containers/ProductsContainer/ProductsContainer.jsx";

function App() {
  return (
    <>
      <ProductsContainer>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<ProductList />} />
            <Route path="/favorites" element={<Favorites />} />
            <Route path="/cart" element={<Cart />} />
          </Routes>
        </BrowserRouter>
      </ProductsContainer>
    </>
  );
}

export default App;
